#!/bin/sh
echo 'PMJ RSS Bot started...'
# get interval variable from config
INTERVAL="$(grep -oE '\$interval\s*=\s*.*;' config.php | tail -1 | sed 's/$interval\s*=\s*//g;s/;//g')"
while [ : ]
do
  # clear
  php -f "bot.php"  #> /dev/null 2>/dev/null
  sleep ${INTERVAL}s
done