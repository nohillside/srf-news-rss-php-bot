<?php
/**
 * Returns a RSS feed as object.
 * @param string $url The feeds url.
 * @param string $interval The interval between calls.
 * @return array An array with the following paramaters: title, link, description, pubDate, language and item(array).
 */
function getRSS($url=null,$interval=5) :array
{
  if (!$url || empty($url)) exit('No RSS feed url configured! Please check your config!'.PHP_EOL);
  $feed = file_get_contents($url);
  $rss  = new SimpleXMLElement($feed);
  // compile output
  $output = array();
  foreach ($rss->channel->item as $item)
  {
    // since the image is also inside the description we need to select everything outside the tag
    preg_match('/(?<=>)(.*)/', (string) $item->description, $description);
    $output[] = array(
      'title' => (string) $item->title,
      'link'  => (string) $item->link,
      'description' => $description[0],
      'pubDate' => (string) $item->pubDate,
      'guid'  => (string) $item->guid,
    );
  }
  // sort by date ascending (oldest first)
  usort($output, function($a, $b) {
    return new DateTime($a['pubDate']) <=> new DateTime($b['pubDate']);
  });
  return $output;
}

/**
 * Toots the text.
 * @param string $text The text to toot.
 * @param string $token Access token from the app.
 * @param string $instance The instance to toot to.
 * @param string $visibility The visbility level for the toot. Can be public, unlisted, private, direct
 * @return object Returns the response from the server api as an object.
 */
function toot($text=null,$token=null,$instance=null,$visibility='unlisted')
{
  // check variables
  if (!$text || empty($text)) exit('No text provided! There\'s nothing to toot!'.PHP_EOL);
  if (!$token || empty($token)) exit('No token provided! Please set the access token in the config!'.PHP_EOL);
  if (!$instance || empty($instance)) exit('Could not connect! Please check your config and set the instance!'.PHP_EOL);
  switch ($visibility)
  {
    case 'public':
    case 'unlisted':
    case 'private':
    case 'direct':
      break;
    default:
      exit('The visbility level is not set correctly! Please check your config and set the visbility to one of the 4 allowed levels!'.PHP_EOL);
  }
  $post = array(
    'status'  => $text,
    'visibility'  => $visibility,
  );
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Bearer '.$token,
    'User-Agent: PMJ\'s PHP Mastodon RSS Bot',
  ));
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_URL, 'https://'.$instance.'/api/v1/statuses');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
  $response = curl_exec($ch);
  curl_close ($ch);
  
  return json_decode($response);
}