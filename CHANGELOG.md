## Changelog

### 1.1

#### 1.1.5 [2023-11-04]

* Version 1.1.5
* Extended dupe check: wait 23h before reposting an URL (by @nohillside)

#### 1.1.4 [2022-11-17]

* Version 1.1.4
* Damn you update check

#### 1.1.3 [2022-11-15]

* Version 1.1.3
* Update checke still doesn't work correctly

#### 1.1.2 [2022-11-15]

* Version 1.1.2
* Update check was in wrong position in code

#### 1.1.1 [2022-11-15]

* Version 1.1.1
* Fixed time check comparison (hopefully)

#### 1.1.0 [2022-11-14]

* Version 1.1.0
* Updated README.md
* Changed mod of bot.sh
* Reworked time check
* Added update check to see if something has already been posted

---
### 1.0

#### 1.0.0 [2022-11-13]

* Version 1.0.0
* pmjrssphpbot.service.sample
* d'err
* bot.sh
* bot.php
* functions.php
* config.php.sample
* README.md
* Initialize repo and add LICENSE
