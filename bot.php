<?php
require_once __DIR__.'/config.php';
require_once __DIR__.'/functions.php';

// files to persist key data between executions
const GUIDCACHE = 'guidcache';
const MARK = 'mark';

// this is only needed on first run and creates the mark file with the current time
if (!file_exists(MARK))
{
  $filestamp = fopen(MARK, 'w');
  fwrite($filestamp, date('r',time()));
  fclose($filestamp);
}

// create/load the cache to record the publishing timestamp of articles
$guidlist = array();
if (file_exists(GUIDCACHE))
{
  $guidlist = json_decode(file_get_contents(GUIDCACHE), true);
}

// get rss feed items
$items = getRSS($feed,$interval);
foreach ($items as $item)
{
  // check if this item is newer than the last post
  $lastpost = strtotime(file_get_contents(MARK, true));
  $itemTime = strtotime($item['pubDate']);
  if ($itemTime <= $lastpost) continue;

  // check if we haven't published it yet
  if (!array_key_exists($item['guid'], $guidlist))
  {
    $text = '+++ '.$item['title'].' +++'.PHP_EOL;
    echo date("Y-m-d H:i", time()).' '.$text;

    $text .= PHP_EOL.$item['description'].PHP_EOL.PHP_EOL;
    $text .= $item['link'];

    toot($text,$accessToken,$instance,$visibility);

    // record publishing timestamp
    $guidlist[$item['guid']] = time();
  }
  else
  {
    $text = date("Y-m-d H:i", time()).' +++ '.$item['title'].' +++ '.date("Y-m-d H:i", $guidlist[$item['guid']]).' SKIPPED'.PHP_EOL;
    echo $text;
  }

  // mark time of this toot
  $filestamp = fopen(MARK, 'w');
  fwrite($filestamp, $item['pubDate']);
  fclose($filestamp);
}

// clean out cached entries published more than 23 hours before
$guidlist = array_filter($guidlist, static function ($element) {
  return $element >= time() - 82800;
});

// persist cache
file_put_contents(GUIDCACHE, json_encode($guidlist));

