# PMJ's PHP Mastodon RSS Bot

This is a very, very, veeery simple bot to post messages from a RSS feed.  
It has been quickly written to replace my old nanojs bots which ran awry.

The bot is written to catch the RSS feed from swiss public broadcaster [SRF](https://srf.ch).  
The format is specified for this feed but you can adapt it to any feed you like.

You can see the Bot in action at <a rel="me" href="https://social.pmj.rocks/@srfnewsrss">@srfnewsrss</a>, <a rel="me" href="https://social.pmj.rocks/@srfsportrss">@srfsportrss</a> and <a rel="me" href="https://social.pmj.rocks/@srfkulturrss">@srfkulturrss</a>

## How to use

* Clone the repo
* Make the bot.sh executable with `chmod +x bot.sh`
* Rename `config.php.sample` to `config.php` and set the variables
* Copy `pmjrssphpbot.service.sample` to `/etc/systemd/system/your-service-name.service` and change the directories and the user in the file
* Reload the daemon with `systemctl daemon-reload`
* Use the bot with `service your-service-name start|stop|status`

## History

Since Twitter abruptly banned my account for alleged misuse of their API on October 13th 2020, I was cut off from my primary source of news for Switzerland.  
And since I like to have everything in one place, I decided to create a small Bot which fetches the official RSS feed and turns it into neat Mastodon toots.

In April 2022, however, after a system update, the bot started to act erratically and flooded the stream with posts. So I had to turn it off.  
Since I was very busy it took some time to get back to problem and it had to be done quickly, but I could not find the problem.  
So finally I decided to write a quick temporary replacement and since I was already doing stuff in PHP I used that language.

## Credits

* LeonanCarvalho and his [excellent answer](https://stackoverflow.com/a/44420339) on how to turn a PHP script into a service
* rubynorails for [the regex](https://unix.stackexchange.com/a/245264) to get the interval variable out of the config